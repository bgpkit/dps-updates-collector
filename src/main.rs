mod dps_asns;

use std::collections::HashSet;
use std::fs::File;
use std::io::{LineWriter, Write};
use bgpkit_broker::{BgpkitBroker, QueryParams};
use bgpkit_parser::{BgpElem, BgpkitParser, ParserError};
use bgpkit_parser::parser::ElemType;
use bzip2::Compression;
use bzip2::write::BzEncoder;
use env_logger::Env;
use log::{info, warn};
use rayon::prelude::*;
use crate::dps_asns::get_dps_asns;

#[inline(always)]
pub fn option_to_string<T>(o: &Option<T>) -> String
    where
        T: std::fmt::Display,
{
    if let Some(v) = o {
        v.to_string()
    } else {
        String::new()
    }
}

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();
    let sleep_secs: u64 = std::env::var("SLEEP_SECS").unwrap_or("120".to_string()).parse().unwrap();

    let dps_asns = get_dps_asns();

    let mut page = 1;
    // specify two week window
    let mut query = QueryParams::new();
    query = query.start_ts(1627776000); // 2021-08-01T00:00:00 UTC
    query = query.end_ts(1635724800); // 2021-11-01T00:00:00 UTC
    query = query.data_type("update");
    query = query.page(page);
    query = query.page_size(num_cpus::get() as i64);

    let f = File::create("output.txt.bz2").unwrap();
    let writer = BzEncoder::new(f, Compression::default());
    let mut buf_writer = LineWriter::new(writer);

    let broker = BgpkitBroker::new("https://api.broker.bgpkit.com/v1");
    loop {
        let res = broker.query(&query).unwrap();
        let data = res.data.unwrap();
        info!("processing page {}/{}, {} files in this page", page, data.total_pages, data.items.len());
        let elems = data.items.par_iter().map(|x|{
            let url = x.url.clone();
            let collector_id = x.collector_id.clone();

            let mut elems  = vec![];

            let parser = match BgpkitParser::new(url.as_str()){
                Ok(p) => {p}
                Err(e) => {
                    if let ParserError::RemoteIoError(e) = e {
                        // reqwest call failed
                        warn!("reqeust to remote resource failed: {}", e.to_string());
                        warn!("thread sleep for {} seconds", sleep_secs);

                        std::thread::sleep(std::time::Duration::from_secs(sleep_secs));

                        warn!("retrying once");

                        match BgpkitParser::new(url.as_str()) {
                            Ok(p) => p,
                            Err(_) => {
                                warn!("request to remote resource failed again: {}", e.to_string());
                                warn!("skipping file: {}", url.as_str());
                                return vec![]
                            }
                        }
                    } else {
                        return vec![]
                    }
                }
            };

            for elem in parser {
                if let ElemType::ANNOUNCE = elem.elem_type {
                    if let Some(as_path) = &elem.as_path {
                        if let Some(origins) = as_path.get_origin() {
                            let origins_set: HashSet<u32> = HashSet::from_iter(origins.clone());
                            if origins_set.intersection(&dps_asns).count()>0 {
                                // found interested elem
                                // warn!("{}", elem);
                                elems.push((collector_id.clone(), elem));
                            }
                        }
                    }
                }
            }
            elems
        }).flat_map(|x|x).collect::<Vec<(String, BgpElem)>>();

        for (collector_id, elem) in &elems {
            let _ = buf_writer.write_all(format!("{}|{}|{}|{}\n", collector_id, elem.timestamp,elem.prefix, option_to_string(&elem.as_path)).as_bytes());
        }
        let _ = buf_writer.flush();

        if data.total_pages>page {
            page += 1;
            query = query.page(page);
        } else {
            break
        }
    }
}
