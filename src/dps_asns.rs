use std::collections::HashSet;

pub fn get_dps_asns() -> HashSet<u32> {
    HashSet::from_iter([
        8757,
        47581,
        13335,
        14789,
        132892,
        133877,
        139242,
        202623,
        203898,
        209242,
        394536,
        395747,
        62571,
        19551,
        6500,
        13117,
        45474,
        63005,
        54113,
        394192,
        55002,
        398349,
        20052,
        10690,
        134060,
        49612,
        57724,
        264409,
        201029,
        394009,
        19324,
        48851,
        15823,
        25773,
        7786,
        12008,
        19905,
        19907,
        19910,
        19911,
        32978,
        32979,
        32980,
        32981,
        38347,
        46823,
        134476,
        138265,
        397213,
        397214,
        397215,
        397216,
        397217,
        397218,
        397219,
        397220,
        397221,
        397222,
        397223,
        397224,
        397225,
        397226,
        397227,
        397228,
        397229,
        397230,
        397231,
        397232,
        397233,
        397234,
        397235,
        397236,
        397237,
        397238,
        397239,
        397240,
        397241,
        397242,
        397243,
        399151,
        399152,
        399153,
        399154,
        399155,
        399156,
        399157,
        399158,
        399159,
        399160,
        399161,
        399162,
        399163,
        399164,
        399165,
        399166,
        399167,
        399168,
        399169,
        399170,
        399171,
        399172,
        399173,
        399174,
        399175,
        399176,
        399177,
        399178,
        399179,
        399180,
        3598,
        5761,
        6182,
        6194,
        6291,
        6584,
        8068,
        8069,
        8070,
        8071,
        8072,
        8073,
        8074,
        8075,
        8812,
        12076,
        13399,
        13811,
        14719,
        17345,
        20046,
        22692,
        23468,
        25796,
        26222,
        30135,
        30575,
        31792,
        32476,
        35106,
        36006,
        40066,
        45139,
        58862,
        59067,
        63314,
        200517,
        395496,
        395524,
        395851,
        396463,
        397466,
        397996,
        398575,
        398656,
        398657,
        398658,
        398659,
        398660,
        398661,
        398961,
        6432,
        13949,
        15169,
        16550,
        16591,
        19425,
        19448,
        19527,
        22577,
        22859,
        26684,
        26910,
        36039,
        36040,
        36384,
        36385,
        36492,
        36987,
        40873,
        41264,
        43515,
        45566,
        55023,
        139070,
        139190,
        394507,
        394639,
        395973,
        396982,
        699,
        7224,
        8987,
        9059,
        10124,
        14618,
        16509,
        17493,
        19047,
        36263,
        38895,
        39111,
        40045,
        58588,
        62785,
        135630,
        395343,
        399834,
        399991,
        12222,
        16625,
        16702,
        17204,
        17334,
        18680,
        18717,
        20189,
        20940,
        21342,
        21357,
        21399,
        22207,
        22452,
        23454,
        23455,
        23903,
        24319,
        26008,
        30675,
        31107,
        31108,
        31109,
        31110,
        31377,
        31984,
        32787,
        33047,
        33905,
        34164,
        34850,
        35204,
        35993,
        35994,
        36029,
        36183,
        39836,
        43639,
        45700,
        45757,
        48163,
        49249,
        49846,
        55409,
        55770,
        133103,
        200005,
        213120,
        393234,
        393560,
        90,
        792,
        793,
        794,
        1215,
        1216,
        1217,
        1218,
        1219,
        1630,
        3457,
        4184,
        4191,
        4192,
        6142,
        7160,
        10884,
        11049,
        11479,
        11506,
        11625,
        11887,
        13832,
        14506,
        14544,
        14919,
        15135,
        15179,
        15519,
        18837,
        18916,
        20037,
        20054,
        22435,
        23885,
        24185,
        29976,
        31898,
        31925,
        33517,
        36282,
        38538,
        39467,
        40921,
        41900,
        43894,
        43898,
        46403,
        46558,
        52019,
        54253,
        57748,
        60285,
        63295,
        136025,
        138207,
        200705,
        200981,
        206209,
        393676,
        393773,
        395010,
        395738,
        399966,
    ])
}