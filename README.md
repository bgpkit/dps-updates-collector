# dps-updates-collector

Collect BGP updates originated from DPS ASes.

## Run

```bash
cargo run --release
```

## Configure Failed Sleep Seconds

use `SLEEP_SECS` environment variable to configure number of seconds to sleep if request failed.

In the following example, the process will sleep for 2 minutes before retrying a failed request.
```bash
SLEEP_SECS=120 cargo run --release
```

The default is 120 seconds.
